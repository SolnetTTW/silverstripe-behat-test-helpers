<?php
namespace Solnet\Test;

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use SilverStripe\BehatExtension\Context\SilverStripeContext;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Core\ClassInfo;
use SilverStripe\Core\Resettable;
use SilverStripe\ORM\DataObject;

/**
 * Defines application features from the specific context.
 *
 * Overrides SilverStripeContext to ensure resizeWindow isn't called
 * as this interferes with Appium testing.
 */
class FeatureContext extends SilverStripeContext implements Context
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct(array $parameters = null)
    {
        parent::__construct($parameters);
    }

    /**
     * Copy of method from SilverStripeContext with the resizeWindow part commented out.
     *
     * @BeforeScenario
     * @param BeforeScenarioScope $event
     */
    public function before(BeforeScenarioScope $event)
    {
        if (!isset($this->databaseName)) {
            throw new \LogicException(
                'Context\'s $databaseName has to be set when implementing SilverStripeAwareContextInterface.'
            );
        }

        $state = $this->getTestSessionState();
        $this->testSessionEnvironment->startTestSession($state);

        // Optionally import database
        if (!empty($state['importDatabasePath'])) {
            $this->testSessionEnvironment->importDatabase(
                $state['importDatabasePath'],
                !empty($state['requireDefaultRecords']) ? $state['requireDefaultRecords'] : false
            );
        } elseif (!empty($state['requireDefaultRecords']) && $state['requireDefaultRecords']) {
            $this->testSessionEnvironment->requireDefaultRecords();
        }

        // Fixtures
        $fixtureFile = (!empty($state['fixture'])) ? $state['fixture'] : null;
        if ($fixtureFile) {
            $this->testSessionEnvironment->loadFixtureIntoDb($fixtureFile);
        }

        // if ($screenSize = Environment::getEnv('BEHAT_SCREEN_SIZE')) {
        //     list($screenWidth, $screenHeight) = explode('x', $screenSize);
        //     $this->getSession()->resizeWindow((int)$screenWidth, (int)$screenHeight);
        // } else {
        //     $this->getSession()->resizeWindow(1024, 768);
        // }

        // Reset everything
        foreach (ClassInfo::implementorsOf(Resettable::class) as $class) {
            $class::reset();
        }
        DataObject::flush_and_destroy_cache();
        DataObject::reset();
        if (class_exists(SiteTree::class)) {
            SiteTree::reset();
        }
    }
}
