<?php

namespace Solnet\Test;

use \Behat\Behat\Context\Context;
use \BrowserStack\Local;
use \BrowserStack\LocalException;

/**
 * Behat context that sets up a BrowserStackLocal process prior to testing, and
 * shuts it down afterwards.
 *
 * This enables Selenium tests to be run on BrowserStack using URLs that refer to
 * the local host, as otherwise BrowserStack would not be able to access eg.
 * http://foobar.127.0.0.1.nip.io/
 *
 * This relies on the BROWSERSTACK_ACCESS_KEY environment variable being set in
 * the running environment. The solnet/silverstripe-docker package will guide
 * this configuration when creating the development environment.
 *
 * NOTE: The @ lines in the comments are required for Behat to run these functions
 * at the appropriate time.
 */

class BrowserStackLocalContext implements Context
{
    private static $browserStackLocal;

    /**
     * Starts the BrowserStackLocal process.
     *
     * @BeforeSuite
     */
    public static function setup()
    {
        echo "Starting BrowserStackLocal";
        self::$browserStackLocal = new Local();
        self::$browserStackLocal->start(
            [
                'key' => getenv('BROWSERSTACK_ACCESS_KEY'),
                'force' => true,
            ]
        );
    }

    /**
     * Shuts down the BrowserStackLocal process.
     *
     * @AfterSuite
     */
    public static function tearDown()
    {
        if (self::$browserStackLocal) {
            echo "Stopping BrowserStackLocal";
            self::$browserStackLocal->stop();
        }
    }
}
