# Solnet Behat test helpers

## Solnet team details

Tech lead: Darren Inwood (darren.inwood@solnet.co.nz)

## Overview of the project

Solnet uses Browserstack to run automated Selenium and Appium tests.  For PHP projects,
these can be run via Behat, which allows writing feature files in Gherkin syntax and
also enabled writing the Selenium/Appium commands to match the Gherkin in PHP.

This package configures your environment to run tests against the Solnet BrowserStack account, and
is a place for storing common reusable Behat "step definitions" to enable a common
Gherkin usage between projects for common operations.

## Requirements

* SilverStripe 4.x

## Installation

It is recommended to install the ''solnet/silverstripe-test-helpers'' metapackage, which
installs both the Behat and PHPUnit repositories.  This will also mean that if in future
more test helpers are written (eg. for JavaScript aspects) these will be installed and
used automatically.

```composer install --dev solnet/silverstripe-test-helpers```

### behat.yml

The behat test runner will look for a behat.yml configuration file in the following
locations relative to the project/module root - the first file found will be used:

* ./mysite/behat.yml
* ./app/behat.yml
* ./behat.yml

## Configuration

The following ''behat.yml'' configures Behat to use the classes supplied by this module,
and run tests using Solnet's BrowserStack Automate account.

```
default:
  suites:
    default:
      contexts:
        # Disables resizeWindow that interferes with Appium (mobile) testing
        - Solnet\Test\FeatureContext
        # Starts and stops BrowserStackLocal tunnel, enables BrowserStack to see localhost
        - Solnet\Test\BrowserStackLocalContext
      paths:
        # Folder containing *.feature files to run as tests
        - %paths.base%/tests/behat/features/
  extensions:
    SilverStripe\BehatExtension\MinkExtension:
      sessions:
        my_session:
          # Tells Mink to use the special BrowserStack selenium2 driver
          browser_stack:
            capabilities:
              extra_capabilities:
                # Sent to BrowserStack to enable the BrowserStackLocal tunnel.
                # Other capabilities can be added here:
                # https://www.browserstack.com/app-automate/capabilities
                "browserstack.local": true
      default_session: my_session
      javascript_session: my_session
    SilverStripe\BehatExtension\Extension:
      bootstrap_file: vendor/silverstripe/cms/tests/behat/serve-bootstrap.php
      screenshot_path: "%paths.base%/tests/_reports/behat/screenshots"
      retry_seconds: 4
```

The ''%paths.base%'' will resolve to the folder that the ''behat.yml'' file is in.

### Environment variables

The following environment variables are used by this module to enable running tests
against BrowserStack:

* BROWSERSTACK_USERNAME - The Solnet BrowserStack account name.
* BROWSERSTACK_ACCESS_KEY - The Solnet BrowserStack account's API key.
* BROWSERSTACK_NAME - The name of the user running tests. This is displayed in BrowserStack's web UI.
* BROWSERSTACK_PROJECT - The name of the project the tests are for. Allows filtering in the web UI.

If you use the ''solnet/silverstripe-docker'' package to create your development environment,
you will be guided through setting these up. (Recommended)

### Profiles

The top indent level of the ''behat.yml'' file contains Behat 'profiles'.  The
''default'' profile above contains the base settings; all other profiles will 
inherit the ''default'' settings, and you can include only those settings that you
wish to override for each subsequent profile.

This is useful for specifying various OS/Browser combinations to send to BrowserStack.
In the following example, we specify two browser/OS combinations that override the
''capabilities'' settings for the BrowserStack session.

```
default:
  # [as above]

profile1:
  extensions:
    SilverStripe\BehatExtension\MinkExtension:
      sessions:
        my_session:
          browser_stack:
            capabilities:
              browser: chrome

profile2:
  extensions:
    SilverStripe\BehatExtension\MinkExtension:
      sessions:
        my_session:
          browser_stack:
            capabilities:
              browser: IE
              version: 11
```

The various ''capabilities'' that you can override are given here:

https://www.browserstack.com/app-automate/capabilities

Note that if you get an error attempting to use a parameter under ''capabilities'', try them
under ''extra_capabilities'' instead.

### Suites

Inside any profile, you can specify one or more suites.  A suite is a set of paths to folders
containing feature files, and also the ''Context'' classes that will be used to provide step
definitions for your Gherkin features.

All profiles will inherit the suites from the ''default'' profile, but you can override this,
and add your own suites on a per-browser basis.  This can be handy for specifying particular
tests that are only run on eg. iPhone.  An example configuration for this might be:

```
default:
  # [as above]

iphone:
  suites:
    default:
      paths:
        - %paths.base%/tests/behat/features/iphone-only/
  extensions:
    SilverStripe\BehatExtension\MinkExtension:
      sessions:
        my_session:
          browser_stack:
            capabilities:
              device: "iPhone X"
              os_version: ""
              resolution: ""
              extra_capabilities:
                real_mobile: "true"

```

The above overrides the ''paths'' setting of the default suite, so that only feature files
in the ''iphone-only'' folder are run for this profile.

If you have groups of tests you need to run frequently against a certain target, it can be
useful to set up a suite just for this.  Note that you can  specify individual feature
files under ''paths'' as well as folders.

### Contexts

Each suite can specify any number of ''Context'' classes, all of which are loaded before running
that suite.

The two main things that ''Context'' classes are useful for are:

1. Providing step definitions for Gherkin language (explained below)
2. Running code before/after running suites, scenarios, and/or features

It is a good idea to use the following contexts provided by SilverStripe, as they
provide many useful step definitions:

```
default:
  suites:
    default:
      contexts:
        # Generic steps like following anchors, pressing buttons, using forms
        - SilverStripe\BehatExtension\Context\BasicContext
        # Catches all emails and provides steps for testing email sending
        - SilverStripe\BehatExtension\Context\EmailContext
        # Provides steps to test logged in/out functionality
        - SilverStripe\BehatExtension\Context\LoginContext
        # Enables creating database content to test with
        - SilverStripe\BehatExtension\Context\FixtureContext:
          # Path to where fixture .yml files live
          - %paths.base%/tests/behat/features/fixtures/
```

It is a good idea to create a project-specific Context class and include that, so you have
a place to add project-specific step definitions.  This is covered below.

### Feature directories

Each suite also has a set of ''paths'' that will be scanned for ''.feature'' files for that
profile/suite combination.

Paths can be to folders and/or individual files.

Folders will be searched recursively, so it can be a useful idea to subdivide these into
organisational folders as you see fit.  This can help with running particular sets of features
for eg. mobile phones, tablets, desktops, everything on Mac, etc.  An example of this can
be found under ''Suites'' above.

Each feature file must have file extension ''.feature'' in order to be detected by Behat.

## Usage

This package installs a test runner binary at ''vendor/bin/ss-test-behat''.

If you installed this module via the ''solnet/silverstripe-test-helpers'' metapackage (recommended)
then you will also have a ''vendor/bin/ss-test'' test-runner runner.

If your project was created using the ''solnet/silverstripe-recipe-core'' package (recommended)
then you will have a composer script installed that can run the test-runner runner.

These commands will all run the Behat tests configured by your ''behat.yml'':

```
vendor/bin/ss-test-behat [command|profile [suite]] 
vendor/bin/ss-test behat [command|profile [suite]] 
composer test behat [command|profile [suite]] 
```

You can get a list of available commands at any time using the ''help'' command:

```composer test behat help```

You can print out a list of all available step definitions using the ''list'' command:

```composer test behat list```

Otherwise, you can run the ''default'' profile by omitting any extra options, or a specified profile
with an optional suite.  You can run multiple profiles by separating them with a space:

```
# Run tests for the 'default' profile and suite
composer test behat
# Run tests for the 'iphone' profile, 'default' suite
composer test behat iphone
# Run tests for the 'iphone' profile, 'iphone_only' suite
composer test behat iphone phone_only
# Run tests for the chrome, ie11, and edge profiles
composer test behat "chrome ie11 edge"
```

### Database content

A new, completely empty database is spun up for every single Feature file.  This has no content at all,
no pages, no site config, nothing, and so is a great way to run tests as it is not dependant on other
tests working first.

The [SilverStripe Behat docs](https://github.com/silverstripe/silverstripe-behat-extension#fixtures)
have some very good info on how to get data into your database to test against.

### Test output

Tests will be output to the CLI as they are run, but they will also create a separate JUnit format
XML report for each profile/suite combination that is run.

The XML files will be saved as ''[folder behat.yml is in]/tests/_reports/behat/[profile]/[suite].xml''.

## Writing tests

There are two parts to writing tests:

1. Feature files, written in Gherkin
2. Step definitions, written in PHP (running Selenium commands)

When you run the test runner, it will scan for feature files according to the selected profile and suite.
Then each feature file will be interpreted via the Gherkin language.

Each line of Gherkin needs to have a matching **Step definition** that describes what action the
browser should take.  The test runner then runs the step definition for each line of Gherkin in
each feature file.

So, the general process for writing tests is:

1. Write Gherkin scenarios for acceptance criteria in plain English
2. Translate these English steps into step definitions that already exist where possible
3. Write step definitions for undefined lines

### Feature files

Feature files are written in [Gherkin](https://docs.cucumber.io/gherkin/reference/).

The best way to write new tests is to look at existing tests in the ''vendor/silverstripe/admin''
and ''vendor/silverstripe/cms'' folders, and use those as a guide on how to use the in-built
step definitions.

https://github.com/silverstripe/silverstripe-admin/tree/1/tests/behat/features  
https://github.com/silverstripe/silverstripe-cms/tree/4/tests/behat/features  

The built-in step definitions **should** be enough to write most tests.  You can get a list of
all available step definitions using:

```composer test behat list```

It is a good idea to go through the following resources for a grounding on how to write your tests:

* SilverStripe Behat module's [Fixtures docs](https://github.com/silverstripe/silverstripe-behat-extension#fixtures)
* SilverStripe Behat module's [FAQs](https://github.com/silverstripe/silverstripe-behat-extension#faq)
* SilverStripe Behat module's [Cheat Sheet](https://github.com/silverstripe/silverstripe-behat-extension#cheatsheet)

### Step definitions

For any steps that need custom step definitions, it's useful to create your own project-specific
''Context'' class and include it in your default suite's contexts in ''behat.yml''.

```
<?php

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;

class FeatureContext implements Context, SnippetAcceptingContext
{
    public function __construct()
    {
    }
}
```

This file needs to be found by the composer autoloader, so you will probably need to add
it to your project's composer.json file's [autoload](https://getcomposer.org/doc/04-schema.md#autoload)
section.

If you have used ''solnet/silverstripe-recipe-core'' to create your project, this will
have been created and configured for you.

Because this class implements ''SnippetAcceptingContext'', Behat will automatically
add stub methods into it for any undefined Gherkin lines it comes across.

See the [Behat docs](http://docs.behat.org/en/latest/user_guide/context/definitions.html)
for info on how to write the actual contents of your custom step definitions.

## Git branching strategy

This project uses the Solnet Git Branching Strategy, which is Gitflow with Pull Requests
for all merges to the ''develop'' branch.

All development should be done in a feature branch named after the JIRA issue; JIRA issues
should have 'Create branch' buttons that will create the branch for you, but if not, the branch
should be named eg. ''feature/ABCD-1234''.

All commit messages should start with the JIRA ticket number eg. ''ABCD-1234 Added widget editing''.

Commit and push your feature branch, and use the Bitbucket web interface to create a Pull Request
to merge into the ''develop'' branch.

## Tests

This package does not have any tests of its own.

## Deployment

Releases and hotfixes are created using gitflow branches merged into master branch.

To create a new "release" version containing new features, follow this process:

1. Create release branch off ''develop'' named ''release/X.Y.Z'' - use Semver to determine the next version number, typically Y would be incremented and Z set to 0
2. Merge release branch into master
3. Create version number tag ''X.Y.Z'' matching the release branch
4. Merge changes from release branch into develop branch (to pull in hotfixes)
5. Delete release branch (optional)

To create a new "hotfix" version containing bugfixes, follow this process:

1. Create hotfix branch off ''master'' named ''hotfix/X.Y.Z'' - use Semver to determine the next version number, typically Z would be incremented
2. Merge hotfix branch into master
3. Create version number tag ''X.Y.Z'' matching the hotfix branch
4. Merge changes from hotfix branch into develop branch
5. Delete hotfix branch (optional)
